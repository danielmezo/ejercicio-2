# README #

Este es un ejercicio para la asignatura de sistemas de gestión empresarial.

Soy Daniel Mezo, vivo en Sitges y me gustan los deportes como rugby, futbol, basquet, skate y surf. También me interesa la tecnología y principalmente la programación.

![banner-surf-course.png](https://bitbucket.org/repo/yra947/images/185055432-banner-surf-course.png)

![29oct2013_tallervideojuegos_i.jpg](https://bitbucket.org/repo/yra947/images/193618733-29oct2013_tallervideojuegos_i.jpg)

![cursos-de-programacion-de-videojuegos-con-scratch-en-murcia-2016.png](https://bitbucket.org/repo/yra947/images/3291937614-cursos-de-programacion-de-videojuegos-con-scratch-en-murcia-2016.png)

El tipo de videojuegos que me gusta son videojuegos de acción-aventura, los de deportes, de carreras, shoot 'em up, principalmente juegos en los que jugando me divierta.

En las plataformas que me gusta jugar son PS4 y móvil. 

![maxresdefault.jpg](https://bitbucket.org/repo/yra947/images/903392794-maxresdefault.jpg)


Y como uno de mis juegos favoritos:

Uncharted™4: El Desenlace del Ladrón

Los hermanos en peligro
Tres años después de los hechos acaecidos en Uncharted 3: La traición de Drake, Nathan Drake ha dejado atrás la búsqueda de tesoros. Sin embargo, el destino no tarda en llamar a su puerta cuando su hermano Sam reaparece pidiéndole ayuda para salvar su vida, además de ofrecerle participar en una aventura ante la que Nathan no puede resistirse.

Los dos parten a la caza del tesoro perdido del capitán Henry Avery y en busca de Libertalia, el utópico refugio pirata que se halla en lo más profundo de los bosques de Madagascar. Uncharted 4: El desenlace del ladrón embarca al jugador en un viaje alrededor del globo por islas selváticas, grandes ciudades y nevados picos montañosos en busca del tesoro de Avery.

![uncharted-4-two-column-01-ps4-eu-11apr16.png](https://bitbucket.org/repo/yra947/images/1541758768-uncharted-4-two-column-01-ps4-eu-11apr16.png)


La jubilación está sobrevalorada
Nathan Drake
Tras retirarse de la búsqueda de tesoros, Nathan se ve empujado de nuevo al mundo de los ladrones. Movido esta vez por un interés mucho más personal, se embarca en una aventura que lo llevará a recorrer el planeta para desentrañar la histórica conspiración que se oculta tras un fabuloso tesoro pirata. Su mayor aventura pondrá a prueba su capacidad física, su determinación y, en última instancia, lo que está dispuesto a sacrificar para salvar a sus seres queridos.

![uncharted-4-two-column-drake-01-ps4-eu-21sep15.png](https://bitbucket.org/repo/yra947/images/2055442538-uncharted-4-two-column-drake-01-ps4-eu-21sep15.png)

En un futuro me gustaría realizar videojuegos y aplicaciones, ya tengo algunas ideas que empiezo a hacer por mi cuenta.

Para acabar dejo un link d la pagina web d mi madre: http://www.decohappy.com/